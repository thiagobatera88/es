<?php

function ExibirMsg($ret) {

     switch ($ret){
        case -100:
            echo '<div class="alert alert-danger" alert-dismissable fade in>
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
             <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;&nbsp;
              Ocorreu um erro na operação!
              </div>';
            break;
        case -1:
            echo '<div class="alert alert-warning" alert-dismissable fade in>
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
             <span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;&nbsp;
             Favor preencher o(s) campo(s) obrigatório(s)
             </div>';
            break;           
     }
}
