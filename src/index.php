﻿<!DOCTYPE html>
<?php

require_once 'Utils/init.php';
require_once './Controller/MusicaCtrl.php';
require_once '_msg.php';

$ctrl = new MusicaCtrl();

$ret = '';
$search = '';
$anoinicial = '';
$anofinal = '';

if(isset($_GET['btnSearch'])){
    
    $search = isset($_GET['search']) ? $_GET['search'] : '';
    $anoinicial = isset($_GET['anoinicial']) ? $_GET['anoinicial'] : '';
    $anofinal = isset($_GET['anofinal']) ? $_GET['anofinal'] : '';
    
    if($search != ''){
        $query = $ctrl->SearchMusicByName($search);
    }
    else if($anoinicial != '' && $anofinal != ''){
        $query = $ctrl->SearchMusicRangeYear($anoinicial, $anofinal);
    }
    else{
        $query = $ctrl->SearchAllMusic();    
        $response = $client->search($query);
    }
    $response = $client->search($query);
    if($response['hits']['total'] >= 1){
        $results = $response['hits']['hits'];        
    }  
}

?>
<html xmlns="http://www.w3.org/1999/xhtml">    
    <?php include '_head.php'; ?>
    <body>
        <div id="wrapper">            
            <?php
            include '_topo.php';
            ?>  
                <section class="jumbotron">          
                    <div class="container">                       
                       <div class="row text-center">
                            <div class="col-md-12">    
                                 <?php ExibirMsg($ret) ?>
                                <h2> Search - ES</h2>                                                                
                            </div>
                        </div>
                        <hr />
                        <form method="get" action="index.php">                        
                        <div class="row">                                                        
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Pesquisar
                                    </div>                                   
                                 <div class="panel-body">  
                                    <div class="col-md-6">                                       
                                        <div class="form-group">                                            
                                            <label>Nome música</label>                                            
                                            <input type="text" class="form-control" onfocus="if(this.value == '')cleanInputs(2)" id="search" name="search" placeholder="Pesquise pelo nome da música" value="<?php echo $search ?>" />                                                                                        
                                        </div>
                                    </div>
                                    <div class="col-md-3">                                            
                                        <div class="form-group">
                                            <label>Ano inicial</label>                                                                             
                                            <input type="text" class="form-control year num datapree" onfocus="cleanInputs(3)" id="anoinicial" name="anoinicial" maxlength="4" placeholder="Ano inicial" value="<?php echo $anoinicial ?>" />                                                                                                                                                                                                                                
                                        </div>                                                    
                                    </div>                                                                                             
                                    <div class="col-md-3">                                               
                                        <div class="form-group">
                                            <label>Ano final</label>                                                                             
                                            <input type="text" class="form-control year num datapree" onfocus="cleanInputs(3)" id="anofinal" name="anofinal" maxlength="4" placeholder="Ano final" value="<?php echo $anofinal ?>" />                                                                                                                                                                                                                                
                                        </div>                                                  
                                    </div>                                        
                               </div>                                                                                                                              
                              </div>
                                <button class="btn btn-primary" title="Pesquisar" name="btnSearch"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Pesquisar</button>                                                       
                                <button class="btn btn-default" onclick="return cleanInputs(1)"><span class="glyphicon glyphicon-refresh"></span>&nbsp;&nbsp;Limpar</button>        
                            </div>                            
                        </div>  
                    </form>
                    <hr />
                    <?php if(isset($results)) { ?>
                         <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Resultado <?= '&nbsp;&nbsp;<b>( ' . count($results) . ' no total )</b>' ?>
                                    </div>                                    
                                    <div class="panel-body">                                                                                  
                                        <div class="table-responsive">                                        
                                            <table class="table table-striped table-bordered table-hover" id="id_tabela">
                                                <thead>
                                                    <tr>
                                                        <th>Nome Música</th>                                                                                                            
                                                        <th>Cantor</th>                                                                                                            
                                                        <th>Ano</th>
                                                        <th>Álbum</th>                                                                                                                                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>                                                
                                                <?php for ($i = 0; $i < count($results); $i++) { ?>
                                                        <tr class="odd gradeX">
                                                            <td><?= $results[$i]['_source']['nome'] ?></td>
                                                            <td><?= $results[$i]['_source']['cantor'] ?></td>                                                    
                                                            <td><?= $results[$i]['_source']['ano'] ?></td>
                                                            <td><?= $results[$i]['_source']['album'] ?></td>                                                                                                                                                                                    
                                                        </tr>
                                                 <?php } ?>
                                                </tbody>
                                            </table>                
                                        </div>                                        
                                    </div>
                                </div>
                            </div>                        
                        </div>
                    <?php } else { ?>
                        <center><div class="alert alert-info">Nenhum registro encontrado</div></center>
                    <?php } ?> 
                    </div>
                </section>    
                <footer class="text-muted">
                    <div class="container">            
                        <center>
                            <p>Thiago Almeida &copy; 2019 - <i>(43)99611-0032 | thiagoalmeida_bsm@outlook.com</i></p>
                        </center>                
                    </div>
                </footer>
            </div>
        <!-- /. WRAPPER  -->           
    </body>
</html>
