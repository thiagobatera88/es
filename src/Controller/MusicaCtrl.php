<?php

class MusicaCtrl{
    
    public function SearchAllMusic() {
        
        $query = [
            'index' => 'disco'       
        ]; 
        return $query;
    }
    
    public function SearchMusicByName($search) {
        
        $query = [
            'index' => 'disco' ,
                'type' => 'musica',
                    'body' => [
                        'query' => [              
                            'match' => [
                                'nome' => $search
                            ]
                        ]
                    ]
                ];        
        return $query;
    }
    
    public function SearchMusicRangeYear($anoinicial, $anofinal) {
      
        $query = [        
            'type' => 'musica', 
                'body' => [
                    'query' => [        
                        'constant_score' => [
                            'filter' => [
                                'range' => [
                                    'ano' => [
                                        'gte' => $anoinicial,
                                        'lte' => $anofinal
                                        ]
                                    ]
                                ]      
                            ]
                        ]
                    ]
                ];        
        return $query;
    }
}
